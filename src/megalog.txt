New iteration
{'af': ReLU(), 'channels': 5, 'conv_layers': 10, 'linear_layers': 10}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9929333090782165
Mean loss: 0.03125690189190209

New iteration
{'af': ELU(alpha=1.0), 'channels': 3, 'conv_layers': 10, 'linear_layers': 8}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9908999919891357
Mean loss: 0.03872518052812666

New iteration
{'af': ReLU(), 'channels': 1, 'conv_layers': 8, 'linear_layers': 1}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.8678333401679993
Mean loss: 0.4353674650192261

New iteration
{'af': ReLU(), 'channels': 6, 'conv_layers': 8, 'linear_layers': 2}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9936833262443543
Mean loss: 0.05542001510038972

New iteration
{'af': ELU(alpha=1.0), 'channels': 8, 'conv_layers': 1, 'linear_layers': 8}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9916999816894532
Mean loss: 0.1442443807143718

New iteration
{'af': ReLU(), 'channels': 6, 'conv_layers': 8, 'linear_layers': 2}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.993833315372467
Mean loss: 0.044648511055856946

New iteration
{'af': ReLU(), 'channels': 8, 'conv_layers': 8, 'linear_layers': 4}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9944833159446717
Mean loss: 0.11333869583904743

New iteration
{'af': ReLU(), 'channels': 9, 'conv_layers': 7, 'linear_layers': 1}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9856833338737487
Mean loss: 0.07284102067351342

New iteration
{'af': ELU(alpha=1.0), 'channels': 7, 'conv_layers': 6, 'linear_layers': 8}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9946666479110717
Mean loss: 0.10121675524860621

New iteration
{'af': ELU(alpha=1.0), 'channels': 3, 'conv_layers': 4, 'linear_layers': 10}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9928166627883911
Mean loss: 0.03442735387943685

New iteration
{'af': ELU(alpha=1.0), 'channels': 4, 'conv_layers': 4, 'linear_layers': 4}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9939833164215088
Mean loss: 0.02948353234678507

New iteration
{'af': ELU(alpha=1.0), 'channels': 7, 'conv_layers': 7, 'linear_layers': 9}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9943166494369506
Mean loss: 0.03404001276940107

New iteration
{'af': ELU(alpha=1.0), 'channels': 10, 'conv_layers': 9, 'linear_layers': 1}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9905499696731568
Mean loss: 0.04802479315549135

New iteration
{'af': ReLU(), 'channels': 7, 'conv_layers': 7, 'linear_layers': 10}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9905833244323731
Mean loss: 262.8777926295996

New iteration
{'af': ELU(alpha=1.0), 'channels': 10, 'conv_layers': 1, 'linear_layers': 1}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Mean accuracy: 0.9365333199501038
Mean loss: 0.3362352907657623

Done with BO
[[0.95212905 0.96015739 0.48756353 1.        ]
 [0.96691812 0.70048561 0.29827623 0.        ]
 [0.75672493 0.05039872 0.0224297  1.        ]
 [0.70986314 0.16161399 0.59891014 1.        ]
 [0.07449894 0.76977243 0.85811291 0.        ]
 [0.75148158 0.15364287 0.60795098 1.        ]
 [0.70894658 0.35083515 0.8555248  1.        ]
 [0.61568825 0.         0.89242621 1.        ]
 [0.59123146 0.79620148 0.70149813 0.        ]
 [0.31013133 0.95858027 0.26414777 0.        ]
 [0.35193581 0.37637899 0.44265072 0.        ]
 [0.60047661 0.80171064 0.7287422  0.        ]
 [0.821239   0.05088825 1.         0.        ]
 [0.66626696 0.93041893 0.76183064 1.        ]
 [0.         0.         1.         0.        ]]
[[-0.99293331]
 [-0.99089999]
 [-0.86783334]
 [-0.99368333]
 [-0.99169998]
 [-0.99383332]
 [-0.99448332]
 [-0.98568333]
 [-0.99466665]
 [-0.99281666]
 [-0.99398332]
 [-0.99431665]
 [-0.99054997]
 [-0.99058332]
 [-0.93653332]]

Param list:
[{'linear_layers': 2, 'conv_layers': 4, 'channels': 4, 'af': ELU(alpha=1.0)}, {'linear_layers': 5, 'conv_layers': 10, 'channels': 9, 'af': ELU(alpha=1.0)}, {'linear_layers': 10, 'conv_layers': 9, 'channels': 6, 'af': ELU(alpha=1.0)}, {'linear_layers': 6, 'conv_layers': 7, 'channels': 2, 'af': ReLU()}, {'linear_layers': 9, 'conv_layers': 10, 'channels': 2, 'af': ReLU()}, {'linear_layers': 7, 'conv_layers': 9, 'channels': 7, 'af': ELU(alpha=1.0)}, {'linear_layers': 5, 'conv_layers': 4, 'channels': 8, 'af': ELU(alpha=1.0)}, {'linear_layers': 8, 'conv_layers': 7, 'channels': 6, 'af': ELU(alpha=1.0)}, {'linear_layers': 2, 'conv_layers': 3, 'channels': 2, 'af': ELU(alpha=1.0)}, {'linear_layers': 6, 'conv_layers': 3, 'channels': 5, 'af': ELU(alpha=1.0)}, {'linear_layers': 4, 'conv_layers': 10, 'channels': 7, 'af': ReLU()}, {'linear_layers': 8, 'conv_layers': 8, 'channels': 2, 'af': ReLU()}, {'linear_layers': 9, 'conv_layers': 4, 'channels': 4, 'af': ReLU()}, {'linear_layers': 9, 'conv_layers': 8, 'channels': 7, 'af': ELU(alpha=1.0)}, {'linear_layers': 4, 'conv_layers': 8, 'channels': 4, 'af': ReLU()}]
Running with: {'linear_layers': 2, 'conv_layers': 4, 'channels': 4, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 5, 'conv_layers': 10, 'channels': 9, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 10, 'conv_layers': 9, 'channels': 6, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 6, 'conv_layers': 7, 'channels': 2, 'af': ReLU()}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 9, 'conv_layers': 10, 'channels': 2, 'af': ReLU()}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 7, 'conv_layers': 9, 'channels': 7, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 5, 'conv_layers': 4, 'channels': 8, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 8, 'conv_layers': 7, 'channels': 6, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 2, 'conv_layers': 3, 'channels': 2, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 6, 'conv_layers': 3, 'channels': 5, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 4, 'conv_layers': 10, 'channels': 7, 'af': ReLU()}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 8, 'conv_layers': 8, 'channels': 2, 'af': ReLU()}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 9, 'conv_layers': 4, 'channels': 4, 'af': ReLU()}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 9, 'conv_layers': 8, 'channels': 7, 'af': ELU(alpha=1.0)}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
Running with: {'linear_layers': 4, 'conv_layers': 8, 'channels': 4, 'af': ReLU()}
Done with fold 1/5
Done with fold 2/5
Done with fold 3/5
Done with fold 4/5
Done with fold 5/5
0.9528112393545817 0.9681726859276554

Process finished with exit code 0
