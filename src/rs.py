import os, sys

#os.chdir(sys.path[0])

import GPyOpt as go
import numpy as np
from sklearn.model_selection import ParameterSampler
from net import Net
import torch
from torch.nn import ReLU, ELU
from bae import cv_train, cpu, gpu, get_objective
import matplotlib.pyplot as plt

domain = {
	"conv_layers": range(1,11),
	"linear_layers": range(1,11),
	"channels": range(1,10),
	"af": [ELU(),ReLU()]
}

def randomSearch(epochs, batch_size, iterations):
	# Randomize some parameters, number of combinations is number of iterations
	# To randomize this, we use ParameterSampler()
	param_list = list(ParameterSampler(domain, n_iter = iterations, random_state = 69420))
	# Print the random parameters
	print("Param list:")
	print(param_list)
	# Store the best Y's, copy the latest if we don't improve over the iterations
	# We also store the best parameters each iterations, so we can look at this if the need should come
	best_y = []
	current_best = 0
	best_params = []
	# We optimize on loss
	for i, params in enumerate(param_list):
		print("Running with: " + str(params))
		loss_fn = torch.nn.CrossEntropyLoss()
		optim = torch.optim.Adam
		
		loss, acc = cv_train(params, loss_fn, optim, epochs = epochs, batch_size = batch_size)
		
		# Define value to optimize, here we do accuracy
		y = acc
		
		# Add y to best, or simply the first iteration
		if i == 0 or y > current_best:
			current_best = y
			best_y.append(y)
			best_params.append(params)
		# No improvements, add current best iteration to list
		else:
			best_y.append(current_best)
			best_params.append(best_params[i - 1])

	return best_y, best_params

if __name__ == "__main__":
	# Run the randomsearch
	best_y, best_params = randomSearch(domain, 50, 40)
	# Plot the results
	plt.figure(figsize=(15, 10))
	x = np.arange(1, len(best_y)+1)
	plt.plot(x, best_y, "o-", color="red", label="Random Search")
	plt.legend()
	plt.xlabel("Iterations")
	plt.ylabel("Loss")
	plt.grid(True)
	plt.savefig("rs.png")
	plt.show()
