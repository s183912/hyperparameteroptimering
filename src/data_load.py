import os
import torch, torchvision
import numpy as np

np.random.seed(69420)
gpu = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def get_data(device, share: float):
    assert 0 <= share <= 1
    os.makedirs("local_data", exist_ok=True)
    torchvision.datasets.KMNIST("local_data", download=True)
    train = torch.load("local_data/KMNIST/processed/training.pt")
    test = torch.load("local_data/KMNIST/processed/test.pt")
    x_train = train[0].to(device).unsqueeze(1).float()
    y_train = train[1].to(device).long()
    x_test = test[0].to(device).unsqueeze(1).float()
    y_test = test[1].to(device).long()
    n_train = int(len(y_train) * share)
    n_test = int(len(y_test) * share)

    train_shuffle = np.arange(n_train)
    test_shuffle = np.arange(n_test)
    np.random.shuffle(train_shuffle)
    np.random.shuffle(test_shuffle)

    return x_train[:n_train][train_shuffle],\
           x_test[:n_test][test_shuffle],\
           y_train[:n_train][train_shuffle],\
           y_test[:n_test][test_shuffle]




