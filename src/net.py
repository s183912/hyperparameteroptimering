import torch
from torch import nn

class Net(nn.Module):

	def __init__(self, conv_layers: int, linear_layers: int, channels: int, af, bnorm: bool = True, pool: bool = True):

		super().__init__()

		# Konvolutionelle lag
		convs = [
			nn.Conv2d(1, channels, (3, 3), stride=1, padding=1),
		]
		if pool:
			convs.insert(1, nn.MaxPool2d(2, 2))
		if bnorm:
			convs.append(nn.BatchNorm2d(channels))
		for _ in range(conv_layers-1):
			convs.append(nn.Conv2d(channels, channels, (3, 3), stride=1, padding=1))
			convs.append(af)
			if bnorm:
				convs.append(nn.BatchNorm2d(channels))
		self.conv = torch.nn.Sequential(*convs)
		
		# Fuldt forbundne lag
		lins = []
		self.size = 14 * 14 * channels if pool else 28 * 28 * channels
		for _ in range(linear_layers-1):
			lins.append(nn.Linear(self.size, self.size))
			lins.append(af)
			if bnorm:
				lins.append(nn.BatchNorm1d(self.size))
		lins.append(nn.Linear(self.size, 10))

		self.lin = torch.nn.Sequential(*lins)
	
	def forward(self, x):

		x = self.conv(x).view(-1, self.size)
		x = self.lin(x)
		return x







