import torch
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern
from torch.nn import CrossEntropyLoss, ReLU, ELU

from bae import optimize, gpu, gen_batches_idcs
from net import Net
from rs import randomSearch
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from data_load import get_data

def contour(opt):
	X = opt.X[:, :2]
	Y = -opt.Y[:, 0]
	gp = GaussianProcessRegressor(random_state=69420, normalize_y=True, n_restarts_optimizer=100,
								  kernel=Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0), nu=2)).fit(X, Y)

	N = 300
	plot_points = []
	x, y = (np.linspace(0, 1, N),) * 2
	[[plot_points.append([x_, y_]) for y_ in y] for x_ in x]
	z = gp.predict(np.array(plot_points))
	x, y = (np.linspace(1, 11, N),) * 2
	xx, yy = np.meshgrid(x, y)
	z_plot = z.reshape((N, N))

	plt.figure(figsize=(15, 10))
	plt.rcParams.update({'font.size': 12})
	fig, ax = plt.subplots()
	contour = ax.contourf(xx, yy, z_plot, np.arange(z_plot.min()*0.9, z_plot.max()*1.1, .01), extend='both')

	fig.colorbar(contour, ax=ax, label='GP estimate of accuracy')

	ax.plot(X[:, 0] * 10 + 1, X[:, 1] * 10 + 1, 'r*')
	for i in range(X.shape[0]):
		ax.annotate(f"{i+1}: {Y[i]*100:.3} %", (X[i, 0] * 10 + 1, X[i, 1] * 10 + 1))

	# ax.set_title(f'GP fitted to accuracies acquired by {acqs_str[acq_idx]}')
	ax.set_xlabel('Convolutional layers')
	ax.set_ylabel('Linear layers')

	plt.savefig("gp.png")

def test(epochs, batch_size, params):
	x_train, x_test, y_train, y_test = get_data(gpu, 1)
	net = Net(**params).to(gpu)
	net.train()
	loss_fn = CrossEntropyLoss()
	optim = torch.optim.Adam(net.parameters(), lr=1e-3)
	for i in range(epochs):
		for batch_idcs in gen_batches_idcs(len(x_train), batch_size):
			output = net(x_train[batch_idcs])
			loss = loss_fn(output, y_train[batch_idcs])
			optim.zero_grad()
			loss.backward()
			optim.step()
	net.eval()
	accs = []
	for batch_idcs in gen_batches_idcs(len(x_test), batch_size):
		out = net(x_test[batch_idcs])
		accs.append(float((y_test[batch_idcs] == torch.argmax(out, dim=1)).cpu().float().mean()))
	return np.mean(accs)


if __name__ == "__main__":
	epochs = 20
	batch_size = 60
	iters = 15
	opt = optimize(epochs, batch_size, iters-5)
	contour(opt)
	best_opt_params = opt.X[list(opt.Y_best).index(opt.Y_best.max())]
	params = {
		"conv_layers": int(best_opt_params[0]*10)+1,
		"linear_layers": int(best_opt_params[1]*10)+1,
		"channels": int(best_opt_params[2]*9)+1,
		"af": ReLU() if int(best_opt_params[3]) else ELU(),
	}
	test_opt_acc = test(epochs, batch_size, params)
	rs_best, best_rs_params = randomSearch(epochs, batch_size, iters)
	best_rs_params = best_rs_params[rs_best.index(max(rs_best))]
	test_rs_acc = test(epochs, batch_size, best_rs_params)
	print(test_opt_acc, test_rs_acc)

	plt.figure(figsize=(15, 10))
	matplotlib.rc("font", size=20)
	x = np.arange(1, iters + 1)
	plt.plot(x, -opt.Y_best, "o-", color="red", label="Bayesian optimization validation")
	plt.plot(x, rs_best, "o-", color="blue", label="Random search validation")
	plt.hlines(test_opt_acc, 1, iters+1, colors=["red"], label="Best bayesian optimization test")
	plt.hlines(test_rs_acc, 1, iters+1, colors=["blue"], label="Best random search test")
	plt.legend()
	plt.xlabel("Iterations")
	plt.ylabel("Accuracy")
	plt.grid(True)
	plt.savefig("bae.png")
