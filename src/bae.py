import GPyOpt as go
import numpy as np
from data_load import get_data
from net import Net
import torch
from torch.nn import ReLU, ELU
from dataclasses import dataclass
from sklearn import model_selection
import matplotlib.pyplot as plt
from pprint import pprint

cpu = torch.device("cpu")
gpu = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def gen_batches_idcs(size, bsize):
	# Genererer batches
	# Batch = antal billeder den træner på før modellen opdateres
	nbatches = size // bsize
	idcs = np.arange(size)
	np.random.shuffle(idcs)
	for batch in range(nbatches):
		yield idcs[batch * bsize:(batch + 1) * bsize]


def cv_train(params, loss_fn, optim, epochs=100, batch_size=50, K=5):
	# Loader data
	CV = model_selection.KFold(K, shuffle=False)
	# Only uses x_train for cv, as test is used only for final test accuracy
	x_train, x_test, y_train, y_test = get_data(gpu, 1)
	accs, losses = np.empty(K), np.empty(K)
	for k, (train_index, test_index) in enumerate(CV.split(x_train, y_train)):
		net = Net(**params).to(gpu)
		optimizer = optim(net.parameters(), lr=3e-3)
		# Extract training and test set for current CV fold,
		# and convert them to PyTorch tensors
		xtrain = torch.tensor(x_train[train_index, :])
		ytrain = torch.tensor(y_train[train_index])
		xtest = torch.tensor(x_train[test_index, :])
		ytest = torch.tensor(y_train[test_index])

		# Training loop
		net.train()
		for i in range(epochs):
			# Træner over batches
			for batch in gen_batches_idcs(int(xtrain.shape[0]), batch_size):
				output = net(xtrain[batch])
				loss = loss_fn(output, ytrain[batch])
				optimizer.zero_grad()
				loss.backward()
				optimizer.step()

		# Evaluation
		net.eval()
		with torch.no_grad():
			output = net(xtest)
			losses[k] = loss_fn(output, ytest)
			accs[k] = (ytest == torch.argmax(output, dim=1)).float().mean()
		print(f"Done with fold {k+1}/{K}")
		
	return losses.mean(), accs.mean()

# Hyperparametre at optimere over
domain = [
	{"name": "conv_layers", "type": "continuous", "domain": (0, 1)},
	{"name": "linear_layers", "type": "continuous", "domain": (0, 1)},
	{"name": "channels", "type": "continuous", "domain": (0, 1)},
	{"name": "activation_function", "type": "categorical", "domain": (0, 1)},
]

class Color:
	PURPLE = '\033[95m'
	CYAN = '\033[96m'
	DARKCYAN = '\033[36m'
	BLUE = '\033[94m'
	GREEN = '\033[92m'
	YELLOW = '\033[93m'
	RED = '\033[91m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	END = '\033[0m'

# Returnerer en objective-funktion (se notebook) baseret på antal epochs og batch size
def get_objective(epochs=100, batch_size=50):
	def objective(x):
		# Hiver parametre ud
		params = {
			"conv_layers": int(x[0][0]*10)+1,
			"linear_layers": int(x[0][1]*10)+1,
			"channels": int(x[0][2]*9)+1,
			"af": ReLU() if x[0][3] else ELU(),
		}
		print(f"{Color.BOLD}{Color.BLUE}New iteration{Color.END}")
		pprint(params)
		
		# Træner og evaluerer netværk
		loss_fn = torch.nn.CrossEntropyLoss()
		optim = torch.optim.Adam
		
		loss, acc = cv_train(params, loss_fn, optim, epochs, batch_size)
		print(f"Mean accuracy: {acc}")
		print(f"Mean loss: {loss}")
		print()
		return -acc
	
	return objective

def optimize(epochs, batch_size, iterations):
	opt = go.methods.BayesianOptimization(f=get_objective(epochs, batch_size), domain=domain, acquisition_type="EI")
	opt.acquisition.exploration_weight = .1
	opt.run_optimization(iterations)
	print(f"{Color.BLUE}{Color.BOLD}Done with BO{Color.END}")
	print(opt.X)
	print(-opt.Y)
	print()
	return opt


if __name__ == "__main__":
	bae_best = optimize()

